module bitbucket.org/_metalogic_/httptreemux-swagger

go 1.16

require (
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/dimfeld/httptreemux/v5 v5.2.2
	github.com/stretchr/testify v1.7.0
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2
	github.com/swaggo/swag v1.7.1
	golang.org/x/net v0.0.0-20210813160813-60bc85c4be6d // indirect
)
